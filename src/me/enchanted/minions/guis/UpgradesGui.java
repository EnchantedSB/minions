package me.enchanted.minions.guis;

import java.io.IOException;
import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.enchanted.minions.Minions;
import me.enchanted.minions.listeners.Listeners;
import me.enchanted.minions.utils.PlayerData;

public class UpgradesGui implements Listener {

	public static void openInv(Player p, Zombie z) {
		PlayerData data = new PlayerData(p.getUniqueId());
		double speedl = 0;
		double multiplierl = 0;
		for (String keys : data.getMinions()) {
			if (z.getLocation()
					.equals(new Location(Bukkit.getWorld(data.getConfig().getString("minion." + keys + ".world")),
							data.getConfig().getDouble("minion." + keys + ".x"),
							data.getConfig().getDouble("minion." + keys + ".y"),
							data.getConfig().getDouble("minion." + keys + ".z")))) {
				speedl = data.getConfig().getDouble("minion." + keys + ".speed");
				multiplierl = data.getConfig().getDouble("minion." + keys + ".multiplier");

			}
		}

		Inventory inv = Bukkit.createInventory(null, 27, "�7Minion Upgrades");

		for (int i = 0; i < inv.getSize(); i++) {
			ItemStack is = new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte) 1);
			ItemMeta im = is.getItemMeta();
			im.setDisplayName("�7");
			is.setItemMeta(im);

			inv.setItem(i, is);
		}

		String sc = Minions.getInstance().getConfig().getDouble("cost.speed." + (speedl + 1)) == 0 ? "N/A"
				: String.format("%,d", Minions.getInstance().getConfig().getDouble("cost.speed." + (speedl + 1)));

		ArrayList<String> sl = new ArrayList<String>();
		sl.add("�7");
		sl.add("�aSpeed Upgrade �7allows you to �2increase the speed");
		sl.add("�7at which your minion �amines blocks");
		sl.add("�7");
		sl.add(" �2�l* �a�lCURRENT LEVEL�7: �r" + speedl);
		sl.add(" �2�l* �a�lNEXT UPGRADE COST�7: �r$" + sc);
		ItemStack speed = new ItemStack(Material.SUGAR, 1);
		ItemMeta sm = speed.getItemMeta();
		sm.setDisplayName("�2�l[!] �a�lSPEED UPGRADE �7(Click)");
		sm.setLore(sl);
		speed.setItemMeta(sm);

		ArrayList<String> ml = new ArrayList<String>();
		ml.add("�7");
		ml.add("�eMultiplier Upgrade �7allows you to �6increase");
		ml.add("�7the �6amount of money �7that you get by claiming your money");
		ml.add("�7");
		ml.add(" �6�l* �e�lCURRENT LEVEL�7: �r" + multiplierl);
		ml.add(" �6�l* �e�lNEXT UPGRADE COST�7: �r$" + sc);
		ItemStack multipliers = new ItemStack(Material.GOLD_INGOT, 1);
		ItemMeta mm = multipliers.getItemMeta();
		mm.setDisplayName("�6�l[!] �e�lMULTIPLIER UPGRADE �7(Click)");
		mm.setLore(ml);
		multipliers.setItemMeta(mm);

		inv.setItem(11, speed);
		inv.setItem(15, multipliers);

		p.openInventory(inv);
	}

	@EventHandler
	public void onClick(InventoryClickEvent e) {
		if (e.getInventory().getName().equalsIgnoreCase("�7Minion Upgrades")) {
			e.setCancelled(true);
			if (e.getSlot() == 11) {
				SpeedGui.openInv((Player) e.getWhoClicked(),
						Listeners.lastClicked.get(e.getWhoClicked().getUniqueId()));
			} else if (e.getSlot() == 15) {
				MultiplierGui.openInv((Player) e.getWhoClicked(),
						Listeners.lastClicked.get(e.getWhoClicked().getUniqueId()));
			}
		}
	}
}
