package me.enchanted.minions.guis;

import java.io.IOException;
import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import me.enchanted.minions.Minions;
import me.enchanted.minions.listeners.Listeners;
import me.enchanted.minions.utils.PlayerData;
import net.milkbowl.vault.economy.EconomyResponse;

public class MultiplierGui implements Listener {

	public static void openInv(Player p, Zombie center) {
		Inventory inv = Bukkit.createInventory(null, 45, "§e§lUpgrade Multipliers");

		for (int i = 0; i < inv.getSize(); i++) {
			ItemStack is = new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte) 15);
			ItemMeta im = is.getItemMeta();
			im.setDisplayName("§7");
			is.setItemMeta(im);

			inv.setItem(i, is);
		}

		inv.setItem(0, getItem(2, Material.GOLD_INGOT, p, center));
		inv.setItem(1, getItem(3, Material.GOLD_INGOT, p, center));
		inv.setItem(2, getItem(4, Material.GOLD_INGOT, p, center));
		inv.setItem(3, getItem(5, Material.GOLD_INGOT, p, center));
		inv.setItem(4, getItem(6, Material.GOLD_BLOCK, p, center));
		inv.setItem(5, getItem(7, Material.GOLD_INGOT, p, center));
		inv.setItem(6, getItem(8, Material.GOLD_INGOT, p, center));
		inv.setItem(7, getItem(9, Material.GOLD_INGOT, p, center));
		inv.setItem(8, getItem(10, Material.GOLD_INGOT, p, center));
		inv.setItem(17, getItem(11, Material.GOLD_BLOCK, p, center));
		inv.setItem(26, getItem(12, Material.GOLD_INGOT, p, center));
		inv.setItem(25, getItem(13, Material.GOLD_INGOT, p, center));
		inv.setItem(24, getItem(14, Material.GOLD_INGOT, p, center));
		inv.setItem(23, getItem(15, Material.GOLD_INGOT, p, center));
		inv.setItem(22, getItem(16, Material.GOLD_BLOCK, p, center));
		inv.setItem(21, getItem(17, Material.GOLD_INGOT, p, center));
		inv.setItem(20, getItem(18, Material.GOLD_INGOT, p, center));
		inv.setItem(19, getItem(19, Material.GOLD_INGOT, p, center));
		inv.setItem(18, getItem(20, Material.GOLD_INGOT, p, center));
		inv.setItem(27, getItem(21, Material.GOLD_BLOCK, p, center));
		inv.setItem(36, getItem(22, Material.GOLD_INGOT, p, center));
		inv.setItem(37, getItem(23, Material.GOLD_INGOT, p, center));
		inv.setItem(38, getItem(24, Material.GOLD_INGOT, p, center));
		inv.setItem(39, getItem(25, Material.GOLD_INGOT, p, center));
		inv.setItem(40, getItem(26, Material.GOLD_BLOCK, p, center));
		inv.setItem(41, getItem(27, Material.GOLD_INGOT, p, center));
		inv.setItem(42, getItem(28, Material.GOLD_INGOT, p, center));
		inv.setItem(43, getItem(29, Material.GOLD_INGOT, p, center));
		inv.setItem(44, getItem(30, Material.NETHER_STAR, p, center));

		p.openInventory(inv);
	}

	public static ItemStack getItem(int level, Material mat, Player p, Zombie z) {

		ArrayList<String> l = new ArrayList<String>();
		String om = Minions.getInstance().getConfig().getDouble("multipliers." + (level - 1)) + "x";
		String m = Minions.getInstance().getConfig().getDouble("multipliers." + level) + "x";
		
		PlayerData data = new PlayerData(p.getUniqueId());
		double original = 0;
		ItemStack is = null;
		for (String keys : data.getMinions()) {
			if (z.getLocation()
					.equals(new Location(Bukkit.getWorld(data.getConfig().getString("minion." + keys + ".world")),
							data.getConfig().getDouble("minion." + keys + ".x"),
							data.getConfig().getDouble("minion." + keys + ".y"),
							data.getConfig().getDouble("minion." + keys + ".z")))) {
				original = data.getConfig().getDouble("minion." + keys + ".multiplier");
			}
		}

		if(level > original+1) {
			System.out.print(level);
			System.out.print(original+1);
			is = new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte) 14);
			ItemMeta im = is.getItemMeta();
			im.setDisplayName("§c§l§o???");
			is.setItemMeta(im);
		} else if(level < original+1) {
			System.out.print(level);
			System.out.print(original+1);
			is = new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte) 5);
			ItemMeta im = is.getItemMeta();
			im.setDisplayName("§a§lPURCHASED");
			is.setItemMeta(im);
		} else {
			is = new ItemStack(mat);
			
			l.add("§7");
			l.add(" §6§l* §e§lMultiplier: §e+1");
			l.add(" §6§l* §e§lCost: §a$"
					+ String.format("%,d", Minions.getInstance().getConfig().getInt("cost.multipliers." + level)));

			
			ItemMeta im = is.getItemMeta();
			im.setDisplayName("§e§lLEVEL " + level);
			im.setLore(l);
			is.setItemMeta(im);
		}

		return is;
	}

	@EventHandler
	public void onClick(InventoryClickEvent e) {
		if (e.getInventory().getName().equalsIgnoreCase("§e§lUpgrade Multipliers")) {
			e.setCancelled(true);
			Player p = (Player) e.getWhoClicked();
			if (e.getCurrentItem().getItemMeta().getDisplayName().contains("§e§lLEVEL")) {
				int level = getLevel(e.getCurrentItem());

				EconomyResponse r = Minions.econ.withdrawPlayer((OfflinePlayer) e.getWhoClicked(),
						Minions.getInstance().getConfig().getDouble("cost.multipliers." + level));
				if (r.transactionSuccess()) {
					e.getWhoClicked().sendMessage("§aYou have successfully bought Level " + level + " on multipliers!");

					Zombie z = Listeners.lastClicked.get(p.getUniqueId());

					PlayerData data = new PlayerData(e.getWhoClicked().getUniqueId());

					for (String keys : data.getMinions()) {
						if (z.getLocation().equals(new Location(
								Bukkit.getWorld(data.getConfig().getString("minion." + keys + ".world")),
								data.getConfig().getDouble("minion." + keys + ".x"),
								data.getConfig().getDouble("minion." + keys + ".y"),
								data.getConfig().getDouble("minion." + keys + ".z")))) {

							p.playSound(p.getLocation(), Sound.ANVIL_USE, 1, 1);
							ItemStack is = new ItemStack(Material.PAPER);
							ItemMeta im = is.getItemMeta();
							im.setDisplayName("§c§lUPGRADING...");
							is.setItemMeta(im);
							e.setCurrentItem(is);
							e.getInventory().setItem(e.getSlot(), is);
							new BukkitRunnable() {

								@Override
								public void run() {

									p.playSound(p.getLocation(), Sound.LEVEL_UP, 1, 1);
									try {
										data.setMultiplier(z, Minions.getInstance().getConfig().getDouble("multipliers." + level));
										openInv((Player) e.getWhoClicked(), z);
									} catch (IOException e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
									}
								}
							}.runTaskLater(Minions.getInstance(), 20);

						}
					}
				} else {
					e.getWhoClicked().sendMessage("§c§lYou do not have enough to purchase this!");
				}
			} else {
				e.getWhoClicked().sendMessage("§cYou cannot purchase that.");
			}
		}
	}

	public static Integer getLevel(ItemStack is) {
		if (is != null && is.hasItemMeta() && is.getItemMeta().hasDisplayName()) {
			String l = is.getItemMeta().getDisplayName();
			String[] split = l.split(" ");
			if (split[0].equalsIgnoreCase("§e§lLEVEL")) {
				int i = Integer.parseInt(split[1]);
				return i;
			}
		}
		return null;
	}
}
