package me.enchanted.minions.listeners;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.scheduler.BukkitRunnable;
import me.enchanted.minions.Minions;
import me.enchanted.minions.Triple;
import me.enchanted.minions.cmds.MinionCommand;
import me.enchanted.minions.guis.UpgradesGui;
import me.enchanted.minions.utils.PlayerData;
import net.brcdev.shopgui.ShopGuiPlusApi;
import net.minecraft.server.v1_8_R3.BlockPosition;
import net.minecraft.server.v1_8_R3.NBTTagCompound;
import net.minecraft.server.v1_8_R3.PacketPlayOutAnimation;
import net.minecraft.server.v1_8_R3.PacketPlayOutBlockBreakAnimation;

public class Listeners implements Listener {

	String spaces = "�7               ";
	public static HashMap<UUID, Triple<Zombie, Double, Double>> info = new HashMap<UUID, Triple<Zombie, Double, Double>>();
	public static HashMap<UUID, HashMap<Zombie, BukkitRunnable>> tasks = new HashMap<UUID, HashMap<Zombie, BukkitRunnable>>();
	public static HashMap<UUID, Zombie> lastClicked = new HashMap<UUID, Zombie>();

	@EventHandler
	public void placeBlock(BlockPlaceEvent e) {
		try {
			if (e.getItemInHand().getItemMeta().getDisplayName().equals("�e�lMinion �7(Place Down)")) {
				int speed = getMeta(e.getItemInHand(), "speed");
				int multiplier = getMeta(e.getItemInHand(), "multiplier");

				e.setCancelled(true);
				PlayerData data = new PlayerData(e.getPlayer().getUniqueId());
				Location loc = e.getBlockPlaced().getLocation().add(0.5, 0, 0.5);
				if (loc.clone().add(0, 0, 1).getBlock().getType() != Material.AIR) {
					e.getPlayer().sendMessage("�cNot enough space!");
					return;
				}

				if (e.getPlayer().getItemInHand().getAmount() == 1) {
					e.getPlayer().setItemInHand(new ItemStack(Material.AIR));
				} else {
					e.getPlayer().getItemInHand().setAmount(e.getPlayer().getItemInHand().getAmount() - 1);
				}

				Zombie z = (Zombie) e.getPlayer().getWorld().spawnEntity(
						new Location(e.getPlayer().getWorld(), loc.getX(), loc.getY(), loc.getZ()), EntityType.ZOMBIE);
				z.setBaby(true);
				z.setVillager(false);
				z.setPassenger(null);
				noAI(z);
				ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
				SkullMeta sm = (SkullMeta) skull.getItemMeta();
				sm.setOwner(e.getPlayer().getName());
				skull.setItemMeta(sm);
				z.getEquipment().setHelmet(skull);
				z.getEquipment().setChestplate(dyeArmor(new ItemStack(Material.LEATHER_CHESTPLATE, 1), 100, 0, 100));
				z.getEquipment().setLeggings(dyeArmor(new ItemStack(Material.LEATHER_LEGGINGS, 1), 100, 0, 100));
				z.getEquipment().setBoots(dyeArmor(new ItemStack(Material.LEATHER_BOOTS, 1), 100, 0, 100));
				z.getEquipment().setItemInHand(new ItemStack(Material.DIAMOND_PICKAXE));

				Block b = z.getWorld().getBlockAt(z.getLocation().add(0, 0, 1));
				z.getWorld().getBlockAt(z.getLocation().clone().add(0,0,1)).setType(Material.AIR);
				if (e.getItemInHand().getItemMeta().getLore().contains(" �6�l* �e�lType: �r�lIron")) {
					z.getWorld().getBlockAt(z.getLocation().clone().add(0,0,1)).setType(Material.IRON_BLOCK);
				} else if (e.getItemInHand().getItemMeta().getLore().contains(" �6�l* �e�lType: �r�lGold")) {
					z.getWorld().getBlockAt(z.getLocation().clone().add(0,0,1)).setType(Material.GOLD_BLOCK);
				} else if (e.getItemInHand().getItemMeta().getLore().contains(" �6�l* �e�lType: �r�lDiamond")) {
					z.getWorld().getBlockAt(z.getLocation().clone().add(0,0,1)).setType(Material.DIAMOND_BLOCK);
				} else if (e.getItemInHand().getItemMeta().getLore().contains(" �6�l* �e�lType: �r�lEmerald")) {
					z.getWorld().getBlockAt(z.getLocation().clone().add(0,0,1)).setType(Material.EMERALD_BLOCK);
				}

				try {
					data.createMinion(e.getPlayer(), z, speed, multiplier, b);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				setupMinion(e.getPlayer(), z);
			}
		} catch (

		Exception ex) {
		}
	}

	@SuppressWarnings("serial")
	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		Player p = e.getPlayer();
		PlayerData data = new PlayerData(p.getUniqueId());
		if (!data.exists()) {
			try {
				File dir = new File(Minions.getInstance().getDataFolder() + File.separator + "MinionData");
				File file = new File(Minions.getInstance().getDataFolder() + File.separator + "MinionData",
						p.getUniqueId().toString() + ".yml");
				dir.mkdirs();
				file.createNewFile();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		} else {
			new BukkitRunnable() {

				@Override
				public void run() {
					for (String m : data.getMinions()) {
						Zombie z = (Zombie) Bukkit.getWorld(data.getConfig().getString("minion." + m + ".world"))
								.spawnEntity(new Location(
										Bukkit.getWorld(data.getConfig().getString("minion." + m + ".world")),
										data.getConfig().getDouble("minion." + m + ".x"),
										data.getConfig().getDouble("minion." + m + ".y"),
										data.getConfig().getDouble("minion." + m + ".z")), EntityType.ZOMBIE);
						z.setBaby(true);
						z.setVillager(false);
						z.setPassenger(null);
						noAI(z);
						ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
						SkullMeta sm = (SkullMeta) skull.getItemMeta();
						sm.setOwner(data.getConfig().getString("minion." + m + ".owner"));
						skull.setItemMeta(sm);
						z.getEquipment().setHelmet(skull);
						z.getEquipment()
								.setChestplate(dyeArmor(new ItemStack(Material.LEATHER_CHESTPLATE, 1), 100, 0, 100));
						z.getEquipment()
								.setLeggings(dyeArmor(new ItemStack(Material.LEATHER_LEGGINGS, 1), 100, 0, 100));
						z.getEquipment().setBoots(dyeArmor(new ItemStack(Material.LEATHER_BOOTS, 1), 100, 0, 100));
						z.getEquipment().setItemInHand(new ItemStack(Material.DIAMOND_PICKAXE));

						z.getWorld().getBlockAt(z.getLocation().clone().add(0,0,1)).setType(Material.AIR);
						z.getWorld().getBlockAt(z.getLocation().clone().add(0,0,1)).setType(Material.getMaterial(data.getConfig().getString("minion." + m + ".blocktype")));

						if (Minions.zombies.containsKey(p.getUniqueId())) {
							Minions.zombies.get(p.getUniqueId()).add(z);
						} else {
							Minions.zombies.put(p.getUniqueId(), new ArrayList<Zombie>() {
								{
									add(z);
								}
							});
						}
						Minions.blocks.put(z, z.getWorld().getBlockAt(z.getLocation().clone().add(0,0,1)).getType());
						if (info.containsKey(e.getPlayer().getUniqueId())) {
							info.get(p.getUniqueId()).put(z, 0.00, 0.00);
						} else {
							info.put(e.getPlayer().getUniqueId(), new Triple<Zombie, Double, Double>() {
								{
									put(z, 0.00, 0.00);
								}
							});
						}
						BukkitRunnable task = new BukkitRunnable() {
							int stage = 0;

							@Override
							public void run() {
								Material b = Minions.blocks.get(z);
								stage++;
								if (stage < 10) {
									PacketPlayOutAnimation animationPacket = new PacketPlayOutAnimation(
											((CraftEntity) z).getHandle(), 0);
									Bukkit.getOnlinePlayers()
											.forEach(t -> ((CraftPlayer) t).getHandle().playerConnection
													.sendPacket(animationPacket));
									new BukkitRunnable() {

										@Override
										public void run() {
											PacketPlayOutBlockBreakAnimation packet = new PacketPlayOutBlockBreakAnimation(
													getBlockEntityId(z.getWorld().getBlockAt(z.getLocation().clone().add(0,0,1))),
													new BlockPosition(z.getWorld().getBlockAt(z.getLocation().clone().add(0,0,1)).getX(), z.getWorld().getBlockAt(z.getLocation().clone().add(0,0,1)).getY(), z.getWorld().getBlockAt(z.getLocation().clone().add(0,0,1)).getZ()), stage);
											Bukkit.getOnlinePlayers()
													.forEach(p -> ((CraftPlayer) p).getHandle().playerConnection
															.sendPacket(packet));

										}
									}.runTaskLater(Minions.getPlugin(Minions.class), 3);
								} else if (stage == 10) {
									PacketPlayOutAnimation animationPacket = new PacketPlayOutAnimation(
											((CraftEntity) z).getHandle(), 0);
									Bukkit.getOnlinePlayers()
											.forEach(t -> ((CraftPlayer) t).getHandle().playerConnection
													.sendPacket(animationPacket));
									z.getWorld().getBlockAt(z.getLocation().clone().add(0, 0, 1)).setType(Material.AIR);

									int add = 0;
									int r = ThreadLocalRandom.current().nextInt(0, 100 + 1);
									if (r <= 10) {
										add = 1;
									}

									double price = info.get(p.getUniqueId()).getMoney(z)
											+ Double.valueOf(new DecimalFormat("0.00").format(ShopGuiPlusApi
													.getItemStackPriceSell(p, new ItemStack(b))));
									double tokens = info.get(p.getUniqueId()).getTokens(z) + add;

									info.get(p.getUniqueId()).put(z, price, tokens);
								} else if (stage == 14) {
									z.getWorld().getBlockAt(z.getLocation().clone().add(0, 0, 1)).setType(Material
											.getMaterial(data.getConfig().getString("minion." + m + ".blocktype")));
									stage = -1;
									PacketPlayOutBlockBreakAnimation packet = new PacketPlayOutBlockBreakAnimation(
											getBlockEntityId(z.getWorld().getBlockAt(z.getLocation().clone().add(0,0,1))), new BlockPosition(z.getWorld().getBlockAt(z.getLocation().clone().add(0,0,1)).getX(), z.getWorld().getBlockAt(z.getLocation().clone().add(0,0,1)).getY(), z.getWorld().getBlockAt(z.getLocation().clone().add(0,0,1)).getZ()),
											stage);
									Bukkit.getOnlinePlayers().forEach(
											t -> ((CraftPlayer) t).getHandle().playerConnection.sendPacket(packet));
								}
							}
						};

						int tl = 31;
						if (data.getConfig().getDouble("minion." + m + ".x") == z.getLocation().getX()) {
							if (data.getConfig().getDouble("minion." + m + ".y") == z.getLocation().getY()) {
								if (data.getConfig().getDouble("minion." + m + ".z") == z.getLocation().getZ()) {
									tl = tl - data.getConfig().getInt("minion." + m + ".speed");
								}
							}
						}

						task.runTaskTimer(Minions.getInstance(), tl, tl);

						if (tasks.containsKey(e.getPlayer().getUniqueId())) {
							tasks.get(e.getPlayer().getUniqueId()).put(z, task);
						} else {
							tasks.put(e.getPlayer().getUniqueId(), new HashMap<Zombie, BukkitRunnable>() {
								{
									put(z, task);
								}
							});
						}

					}
				}
			}.runTaskLater(Minions.getInstance(), 20);

		}
	}

	@EventHandler
	public void onLeave(PlayerQuitEvent e) {
		Player p = e.getPlayer();
		info.remove(p.getUniqueId());

		for (Zombie z : Minions.zombies.get(p.getUniqueId())) {
			BukkitRunnable t = tasks.get(p.getUniqueId()).get(z);
			t.cancel();
			Minions.blocks.remove(z);
			z.remove();
		}

		Minions.zombies.get(p.getUniqueId()).clear();
		;

	}

	@EventHandler
	public void onClick(PlayerInteractAtEntityEvent e) {
		Player pp = e.getPlayer();
		for (Zombie z : Minions.zombies.get(pp.getUniqueId())) {
			if (e.getRightClicked().equals(z)) {
				if (info.containsKey(pp.getUniqueId())) {
					Double money = info.get(pp.getUniqueId()).getMoney(z);
					Double tokens = info.get(pp.getUniqueId()).getTokens(z);

					lastClicked.put(pp.getUniqueId(), z);
					openGUI(pp, money, tokens);
				}
			}
		}
	}

	private static int getBlockEntityId(Block block) {
		// There will be some overlap here, but these effects are very localized so it
		// should be OK.
		return ((block.getX() & 0xFFF) << 20) | ((block.getZ() & 0xFFF) << 8) | (block.getY() & 0xFF);
	}

	void noAI(Entity bukkitEntity) {
		net.minecraft.server.v1_8_R3.Entity nmsEntity = ((CraftEntity) bukkitEntity).getHandle();
		NBTTagCompound tag = nmsEntity.getNBTTag();
		if (tag == null) {
			tag = new NBTTagCompound();
		}
		nmsEntity.c(tag);
		tag.setInt("NoAI", 1);
		tag.setInt("Silent", 1);
		nmsEntity.f(tag);
	}

	public ItemStack dyeArmor(ItemStack itemstack, int red, int green, int blue) {

		LeatherArmorMeta m = (LeatherArmorMeta) itemstack.getItemMeta();
		m.setColor(Color.fromRGB(red, green, blue));
		itemstack.setItemMeta(m);
		return itemstack;
	}

	@SuppressWarnings("serial")
	@EventHandler
	public void onClick(InventoryClickEvent e) {
		Player p = (Player) e.getWhoClicked();
		PlayerData data = new PlayerData(p.getUniqueId());
		Inventory inv = e.getInventory();
		Zombie z = lastClicked.get(p.getUniqueId());
		if (inv.getTitle().equalsIgnoreCase(spaces + "�4�lMinions")) {
			e.setCancelled(true);
			if (e.getSlot() == 11) {

				double money = info.get(p.getUniqueId()).getMoney(z);

				Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),
						"eco give " + e.getWhoClicked().getName() + " "
								+ (money * Minions.getInstance().getConfig().getDouble(
										"multipliers." + (int) data.getMultiplier(lastClicked.get(p.getUniqueId())))));
				info.get(p.getUniqueId()).put(z, 0.00, info.get(p.getUniqueId()).getTokens(z));

				openGUI(p, 0.00, info.get(p.getUniqueId()).getTokens(z));

				p.sendMessage("�eYou have withdrawn �a$"
						+ withSuffix(money * Minions.getInstance().getConfig()
								.getDouble("multipliers." + (int) data.getMultiplier(lastClicked.get(p.getUniqueId()))))
						+ " �efrom your minion!");
				p.playSound(p.getLocation(), Sound.ORB_PICKUP, 1, 1);
			} else if (e.getSlot() == 15) {

				double tokens = info.get(p.getUniqueId()).getTokens(z);

				Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),
						"tokens add " + e.getWhoClicked().getName() + " "
								+ (tokens * Minions.getInstance().getConfig().getDouble(
										"multipliers." + (int) data.getMultiplier(lastClicked.get(p.getUniqueId())))));
				info.get(p.getUniqueId()).put(z, info.get(p.getUniqueId()).getMoney(z), 0.00);

				openGUI(p, info.get(p.getUniqueId()).getMoney(z), 0.00);

				p.sendMessage("�eYou have withdrawn �6"
						+ withSuffix(tokens * Minions.getInstance().getConfig()
								.getDouble("multipliers." + (int) data.getMultiplier(lastClicked.get(p.getUniqueId()))))
						+ " �6Tokens �efrom your minion!");
				p.playSound(p.getLocation(), Sound.ORB_PICKUP, 1, 1);
			} else if (e.getSlot() == 22) {
				UpgradesGui.openInv(p, z);
			} else if (e.getSlot() == 26) {
				if (Minions.blocks.get(z).equals(Material.IRON_BLOCK)) {
					MinionCommand.giveItem(p, (int) data.getSpeed(lastClicked.get(p.getUniqueId())), (int) data.getMultiplier(lastClicked.get(p.getUniqueId())), "Iron");
				} else if (Minions.blocks.get(z).equals(Material.GOLD_BLOCK)) {
					MinionCommand.giveItem(p, (int) data.getSpeed(lastClicked.get(p.getUniqueId())), (int) data.getMultiplier(lastClicked.get(p.getUniqueId())), "Gold");
				} else if (Minions.blocks.get(z).equals(Material.DIAMOND_BLOCK)) {
					MinionCommand.giveItem(p, (int) data.getSpeed(lastClicked.get(p.getUniqueId())), (int) data.getMultiplier(lastClicked.get(p.getUniqueId())), "Diamond");
				} else if (Minions.blocks.get(z).equals(Material.EMERALD_BLOCK)) {
					MinionCommand.giveItem(p, (int) data.getSpeed(lastClicked.get(p.getUniqueId())), (int) data.getMultiplier(lastClicked.get(p.getUniqueId())), "Emerald");
				}

				p.closeInventory();
				data.removeMinion(z);
				z.remove();
				Minions.zombies.get(p.getUniqueId()).remove(z);
				info.get(p.getUniqueId()).remove(z);
				tasks.get(p.getUniqueId()).get(z).cancel();
				tasks.get(p.getUniqueId()).remove(z);
			}
		}
	}

	public void openGUI(Player p, Double money, Double tokens) {
		PlayerData data = new PlayerData(p.getUniqueId());
		Inventory inv = Bukkit.createInventory(null, 27, spaces + "�4�lMinions");

		for (int i = 0; i < inv.getSize(); i++) {
			ItemStack is = new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte) 1);
			ItemMeta im = is.getItemMeta();
			im.setDisplayName("�7");
			is.setItemMeta(im);
			inv.setItem(i, is);
		}

		double mo = money * Minions.getInstance().getConfig()
				.getInt("multipliers." + (int) data.getMultiplier(lastClicked.get(p.getUniqueId())));
		double to = tokens * Minions.getInstance().getConfig()
				.getInt("multipliers." + (int) data.getMultiplier(lastClicked.get(p.getUniqueId())));
		ItemStack m = new ItemStack(Material.EMERALD);
		ItemMeta mm = m.getItemMeta();
		mm.setDisplayName("�2�l[!] �a�lMoney");
		mm.setLore(Arrays.asList(" �2�l* �a�lCURRENT VALUE�7: �r$" + withSuffix(mo), "�bClick �7to claim"));
		m.setItemMeta(mm);

		ItemStack t = new ItemStack(Material.DOUBLE_PLANT);
		ItemMeta tm = t.getItemMeta();
		tm.setDisplayName("�6�l[!] �e�lTokens");
		tm.setLore(Arrays.asList(" �6�l* �e�lCURRENT VALUE�7: �r" + withSuffix(to), "�bClick �7to claim"));
		t.setItemMeta(tm);

		ItemStack u = new ItemStack(Material.HOPPER);
		ItemMeta um = u.getItemMeta();
		um.setDisplayName("�5�l[!] �d�lUpgrades");
		u.setItemMeta(um);

		ItemStack rm = new ItemStack(Material.BARRIER);
		ItemMeta rmim = rm.getItemMeta();
		rmim.setDisplayName("�4�l[!] �c�lREMOVE MINION �7(Click)");
		rmim.setLore(Arrays.asList("�7", "�7Once clicked, your �cMinion will despawn�7,",
				"�7and you will be placed �ainto your inventory�7.", "�7",
				" �c�oWARNING: �f�oYou will lose your money inside if you remove!"));
		rm.setItemMeta(rmim);

		inv.setItem(11, m);
		inv.setItem(15, t);
		inv.setItem(22, u);
		inv.setItem(26, rm);

		p.openInventory(inv);
	}

	public void setupMinion(Player p, Zombie z) {
		PlayerData data = new PlayerData(p.getUniqueId());
		new BukkitRunnable() {

			@SuppressWarnings("serial")
			@Override
			public void run() {
				if (Minions.zombies.containsKey(p.getUniqueId())) {
					Minions.zombies.get(p.getUniqueId()).add(z);
				} else {
					Minions.zombies.put(p.getUniqueId(), new ArrayList<Zombie>() {
						{
							add(z);
						}
					});
				}

				Minions.blocks.put(z, z.getWorld().getBlockAt(z.getLocation().clone().add(0, 0, 1)).getType());

				if (info.containsKey(p.getUniqueId())) {
					info.get(p.getUniqueId()).put(z, 0.00, 0.00);
				} else {
					info.put(p.getUniqueId(), new Triple<Zombie, Double, Double>() {
						{
							put(z, 0.00, 0.00);
						}
					});
				}
				BukkitRunnable task = new BukkitRunnable() {
					int stage = 0;

					@Override
					public void run() {
						stage++;
						if (stage < 10) {
							PacketPlayOutAnimation animationPacket = new PacketPlayOutAnimation(
									((CraftEntity) z).getHandle(), 0);
							Bukkit.getOnlinePlayers().forEach(
									t -> ((CraftPlayer) t).getHandle().playerConnection.sendPacket(animationPacket));
							new BukkitRunnable() {

								@Override
								public void run() {
									PacketPlayOutBlockBreakAnimation packet = new PacketPlayOutBlockBreakAnimation(
											getBlockEntityId(z.getWorld().getBlockAt(z.getLocation().clone().add(0,0,1))), new BlockPosition(z.getWorld().getBlockAt(z.getLocation().clone().add(0,0,1)).getX(), z.getWorld().getBlockAt(z.getLocation().clone().add(0,0,1)).getY(), z.getWorld().getBlockAt(z.getLocation().clone().add(0,0,1)).getZ()),
											stage);
									Bukkit.getOnlinePlayers().forEach(
											p -> ((CraftPlayer) p).getHandle().playerConnection.sendPacket(packet));

								}
							}.runTaskLater(Minions.getPlugin(Minions.class), 3);
						} else if (stage == 10) {
							PacketPlayOutAnimation animationPacket = new PacketPlayOutAnimation(
									((CraftEntity) z).getHandle(), 0);
							Bukkit.getOnlinePlayers().forEach(
									t -> ((CraftPlayer) t).getHandle().playerConnection.sendPacket(animationPacket));
							z.getWorld().getBlockAt(z.getLocation().add(0, 0, 1)).setType(Material.AIR);

							int add = 0;
							int r = ThreadLocalRandom.current().nextInt(0, 100 + 1);
							if (r <= 10) {
								add = 1;
							}

							double price = info.get(p.getUniqueId()).getMoney(z)
									+ Double.valueOf(new DecimalFormat("0.00").format(
											ShopGuiPlusApi.getItemStackPriceSell(p, new ItemStack(z.getWorld().getBlockAt(z.getLocation().clone().add(0,0,1)).getType()))));
							double tokens = info.get(p.getUniqueId()).getTokens(z) + add;

							info.get(p.getUniqueId()).put(z, price, tokens);
						} else if (stage == 14) {
							for (String m : data.getMinions()) {
								if (data.getConfig().getDouble("minion." + m + ".x") == z.getLocation().getX()) {
									if (data.getConfig().getDouble("minion." + m + ".y") == z.getLocation().getY()) {
										if (data.getConfig().getDouble("minion." + m + ".z") == z.getLocation()
												.getZ()) {
											z.getWorld().getBlockAt(z.getLocation().add(0, 0, 1))
													.setType(Material.getMaterial(
															data.getConfig().getString("minion." + m + ".blocktype")));
										}
									}
								}
							}
							stage = -1;
							PacketPlayOutBlockBreakAnimation packet = new PacketPlayOutBlockBreakAnimation(
									getBlockEntityId(z.getWorld().getBlockAt(z.getLocation().clone().add(0,0,1))), new BlockPosition(z.getWorld().getBlockAt(z.getLocation().clone().add(0,0,1)).getX(), z.getWorld().getBlockAt(z.getLocation().clone().add(0,0,1)).getY(), z.getWorld().getBlockAt(z.getLocation().clone().add(0,0,1)).getZ()), stage);
							Bukkit.getOnlinePlayers()
									.forEach(t -> ((CraftPlayer) t).getHandle().playerConnection.sendPacket(packet));
						}
					}
				};

				int tl = 31;
				for (String m : data.getMinions()) {
					if (data.getConfig().getDouble("minion." + m + ".x") == z.getLocation().getX()) {
						if (data.getConfig().getDouble("minion." + m + ".y") == z.getLocation().getY()) {
							if (data.getConfig().getDouble("minion." + m + ".z") == z.getLocation().getZ()) {
								tl = tl - data.getConfig().getInt("minion." + m + ".speed");
							}
						}
					}
				}

				task.runTaskTimer(Minions.getInstance(), tl, tl);

				if (tasks.containsKey(p.getUniqueId())) {
					tasks.get(p.getUniqueId()).put(z, task);
				} else {
					tasks.put(p.getUniqueId(), new HashMap<Zombie, BukkitRunnable>() {
						{
							put(z, task);
						}
					});
				}
			}

		}.runTaskLater(Minions.getInstance(), 20);
	}

	public static String withSuffix(double d) {
		if (d < 0)
			d = -d;
		if (d < 1000)
			return "" + d;
		int exp = (int) (Math.log(d) / Math.log(1000));
		return String.format("%.2f%c", d / Math.pow(1000, exp), "kMBTqQsS".charAt(exp - 1));
	}

	public Integer getMeta(ItemStack i, String key) {

		net.minecraft.server.v1_8_R3.ItemStack item = CraftItemStack.asNMSCopy(i);

		if (item.getTag() == null) {
			return 1;
		}

		return item.getTag().getInt(key);
	}

}
