package me.enchanted.minions.cmds;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import me.enchanted.minions.utils.PlayerData;
import net.minecraft.server.v1_8_R3.NBTTagCompound;

public class MinionCommand implements CommandExecutor {

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (sender.hasPermission("Minions.access")) {
			if (args.length == 4) {
				String type = args[0];
				if (type.equalsIgnoreCase("iron") || type.equalsIgnoreCase("gold") || type.equalsIgnoreCase("diamond")
						|| type.equalsIgnoreCase("emerald")) {
					if (Bukkit.getPlayer(args[1]) == null) {
						sender.sendMessage("�cThat player is offline!");
						return true;
					}
					Player p = Bukkit.getPlayer(args[1]);
					int speed = Integer.parseInt(args[2]);
					int multiplier = Integer.parseInt(args[3]);

					giveItem(p, speed, multiplier, type);
				} else {
					sender.sendMessage("�6Types");
					sender.sendMessage("�6- Iron");
					sender.sendMessage("�6- Gold");
					sender.sendMessage("�6- Diamond");
					sender.sendMessage("�6- Emerald");
				}
			}
		}

		return true;
	}

	public static void giveItem(Player p, int speed, int multiplier, String type) {
		ItemStack item = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
		item = setMeta(item, speed, multiplier);
		final SkullMeta itemm = (SkullMeta) item.getItemMeta();
		itemm.setDisplayName("�e�lMinion �7(Place Down)");
		itemm.setLore(Arrays.asList("�7�oThis Minion will automatically mine", "�7�oblocks depending on the upgrades.",
				" �6�l* �e�lType: �r�l" + type, " �6�l* �e�lUpgrades:", "   �r- �eSpeed �7" + speed,
				"   �r- �eMultiplier �7" + multiplier, "�7",
				"�7�o(( �r�oPlace Down �7�othis Minion to begin generating money ))"));
		itemm.setOwner(p.getName());
		item.setItemMeta(itemm);

		p.getInventory().addItem(item);
	}

	public static ItemStack setMeta(ItemStack i, int speed, int multiplier) {

		net.minecraft.server.v1_8_R3.ItemStack Itemstack = CraftItemStack.asNMSCopy(i);

		NBTTagCompound compound = new NBTTagCompound();

		compound.setInt("speed", speed);
		compound.setInt("multiplier", multiplier);

		Itemstack.setTag(compound);

		i = CraftItemStack.asBukkitCopy(Itemstack);

		return i;
	}
}