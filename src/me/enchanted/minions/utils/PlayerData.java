package me.enchanted.minions.utils;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.configuration.Configuration;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.entity.Zombie;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import me.enchanted.minions.Minions;
import me.enchanted.minions.listeners.Listeners;
import net.brcdev.shopgui.ShopGuiPlusApi;
import net.minecraft.server.v1_8_R3.BlockPosition;
import net.minecraft.server.v1_8_R3.PacketPlayOutAnimation;
import net.minecraft.server.v1_8_R3.PacketPlayOutBlockBreakAnimation;

public class PlayerData {

	private File file;
	public FileConfiguration config;

	UUID uuid = null;

	public PlayerData(UUID name) {
		uuid = name;
		file = new File(Minions.getInstance().getDataFolder() + "/MinionData", name.toString() + ".yml");
		config = YamlConfiguration.loadConfiguration(file);
	}

	public boolean exists() {
		return file.exists();
	}

	public Configuration getConfig() {
		return config;
	}

	public Set<String> getMinions() {
		return config.getConfigurationSection("minion").getKeys(false);
	}

	public void createMinion(Player p, Zombie z, int speed, int multiplier, Block b) throws IOException {
		UUID random = UUID.randomUUID();
		config.set("minion." + random + ".speed", speed);
		config.set("minion." + random + ".multiplier", multiplier);
		config.set("minion." + random + ".owner", p.getName());
		config.set("minion." + random + ".world", p.getWorld().getName());
		config.set("minion." + random + ".x", z.getLocation().getX());
		config.set("minion." + random + ".y", z.getLocation().getY());
		config.set("minion." + random + ".z", z.getLocation().getZ());
		config.set("minion." + random + ".blocktype", b.getType().name());
		config.save(file);
	}
	
	public void removeMinion(Zombie z) {
		for (String m : getMinions()) {
			if (z.getLocation()
					.equals(new Location(Bukkit.getWorld(getConfig().getString("minion." + m + ".world")),
							getConfig().getDouble("minion." + m + ".x"), getConfig().getDouble("minion." + m + ".y"),
							getConfig().getDouble("minion." + m + ".z")))) {
				config.set("minion." + m, null);
				try {
					config.save(file);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	public double getMultiplier(Zombie z) {
		for (String m : getMinions()) {
			if (z.getLocation()
					.equals(new Location(Bukkit.getWorld(getConfig().getString("minion." + m + ".world")),
							getConfig().getDouble("minion." + m + ".x"), getConfig().getDouble("minion." + m + ".y"),
							getConfig().getDouble("minion." + m + ".z")))) {
				return getConfig().getDouble("minion." + m + ".multiplier");
			}
		}
		return 1;
	}

	public double getSpeed(Zombie z) {
		for (String m : getMinions()) {
			if (z.getLocation()
					.equals(new Location(Bukkit.getWorld(getConfig().getString("minion." + m + ".world")),
							getConfig().getDouble("minion." + m + ".x"), getConfig().getDouble("minion." + m + ".y"),
							getConfig().getDouble("minion." + m + ".z")))) {
				return getConfig().getDouble("minion." + m + ".speed");
			}
		}
		return 1;
	}
	
	public void setMultiplier(Zombie z, double multiplier) throws IOException {
		for (String m : getMinions()) {
			if (z.getLocation()
					.equals(new Location(Bukkit.getWorld(getConfig().getString("minion." + m + ".world")),
							getConfig().getDouble("minion." + m + ".x"), getConfig().getDouble("minion." + m + ".y"),
							getConfig().getDouble("minion." + m + ".z")))) {
				config.set("minion." + m + ".multiplier", multiplier);
				config.save(file);
			}
		}
	}

	public void setSpeed(Zombie z, int speed) throws IOException {
		for (String m : getMinions()) {
			if (z.getLocation()
					.equals(new Location(Bukkit.getWorld(getConfig().getString("minion." + m + ".world")),
							getConfig().getDouble("minion." + m + ".x"), getConfig().getDouble("minion." + m + ".y"),
							getConfig().getDouble("minion." + m + ".z")))) {
				config.set("minion." + m + ".speed", speed);
				config.save(file);

				BukkitRunnable task = new BukkitRunnable() {
					int stage = 0;

					@Override
					public void run() {
						Material b = Minions.blocks.get(z);
						stage++;
						if (stage < 10) {
							PacketPlayOutAnimation animationPacket = new PacketPlayOutAnimation(
									((CraftEntity) z).getHandle(), 0);
							Bukkit.getOnlinePlayers().forEach(
									t -> ((CraftPlayer) t).getHandle().playerConnection.sendPacket(animationPacket));
							new BukkitRunnable() {

								@Override
								public void run() {
									PacketPlayOutBlockBreakAnimation packet = new PacketPlayOutBlockBreakAnimation(
											getBlockEntityId(z.getWorld().getBlockAt(z.getLocation().clone().add(0,0,1))), new BlockPosition(z.getWorld().getBlockAt(z.getLocation().clone().add(0,0,1)).getX(), z.getWorld().getBlockAt(z.getLocation().clone().add(0,0,1)).getY(), z.getWorld().getBlockAt(z.getLocation().clone().add(0,0,1)).getZ()),
											stage);
									Bukkit.getOnlinePlayers().forEach(
											p -> ((CraftPlayer) p).getHandle().playerConnection.sendPacket(packet));

								}
							}.runTaskLater(Minions.getPlugin(Minions.class), 3);
						} else if (stage == 10) {
							PacketPlayOutAnimation animationPacket = new PacketPlayOutAnimation(
									((CraftEntity) z).getHandle(), 0);
							Bukkit.getOnlinePlayers().forEach(
									t -> ((CraftPlayer) t).getHandle().playerConnection.sendPacket(animationPacket));
							Bukkit.getWorld("world").getBlockAt(z.getLocation().add(0, 0, 1)).setType(Material.AIR);
							for (int i = 0; i < 23; i++) {
								double x = ThreadLocalRandom.current().nextDouble(-0.25, 0.25 + 1);
								;
								double y = ThreadLocalRandom.current().nextDouble(-0.25, 0.25 + 1);
								;
								double zc = ThreadLocalRandom.current().nextDouble(-0.25, 0.25 + 1);
								z.getWorld().getBlockAt(z.getLocation().clone().add(0,0,1)).getWorld().playEffect(z.getWorld().getBlockAt(z.getLocation().clone().add(0,0,1)).getLocation().add(0.5 + x, 0.5 + y, 0.5 + zc),
										Effect.TILE_BREAK, 57);
							}

							int add = 0;
							int r = ThreadLocalRandom.current().nextInt(0, 100 + 1);
							if (r <= 10) {
								add = 1;
							}

							double price = Listeners.info.get(uuid).getMoney(z) + Double
									.valueOf(new DecimalFormat("0.00").format(ShopGuiPlusApi.getItemStackPriceSell(
											Bukkit.getPlayer(uuid), new ItemStack(z.getWorld().getBlockAt(z.getLocation().clone().add(0,0,1)).getType()))));
							double tokens = Listeners.info.get(uuid).getTokens(z) + add;

							Listeners.info.get(uuid).put(z, price, tokens);
						} else if (stage == 14) {
							for (String m : getMinions()) {
								if (getConfig().getDouble("minion." + m + ".x") == z.getLocation().getX()) {
									if (getConfig().getDouble("minion." + m + ".y") == z.getLocation().getY()) {
										if (getConfig().getDouble("minion." + m + ".z") == z.getLocation().getZ()) {
											z.getLocation().add(0, 0, 1).getBlock().setType(Material
													.getMaterial(getConfig().getString("minion." + m + ".blocktype")));
										}
									}
								}
							}
							stage = -1;
							PacketPlayOutBlockBreakAnimation packet = new PacketPlayOutBlockBreakAnimation(
									getBlockEntityId(z.getWorld().getBlockAt(z.getLocation().clone().add(0,0,1))), new BlockPosition(z.getWorld().getBlockAt(z.getLocation().clone().add(0,0,1)).getX(), z.getWorld().getBlockAt(z.getLocation().clone().add(0,0,1)).getY(), z.getWorld().getBlockAt(z.getLocation().clone().add(0,0,1)).getZ()), stage);
							Bukkit.getOnlinePlayers()
									.forEach(t -> ((CraftPlayer) t).getHandle().playerConnection.sendPacket(packet));
						}
					}
				};

				task.runTaskTimer(Minions.getInstance(), 31 - speed, 31 - speed);
				Listeners.tasks.get(uuid).get(z).cancel();
				Listeners.tasks.get(uuid).remove(z);
				Listeners.tasks.get(uuid).put(z, task);
			}
		}
	}

	private static int getBlockEntityId(Block block) {
		// There will be some overlap here, but these effects are very localized so it
		// should be OK.
		return ((block.getX() & 0xFFF) << 20) | ((block.getZ() & 0xFFF) << 8) | (block.getY() & 0xFF);
	}
}
