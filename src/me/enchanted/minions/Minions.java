package me.enchanted.minions;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import me.enchanted.minions.cmds.MinionCommand;
import me.enchanted.minions.guis.MultiplierGui;
import me.enchanted.minions.guis.SpeedGui;
import me.enchanted.minions.guis.UpgradesGui;
import me.enchanted.minions.listeners.Listeners;
import me.enchanted.minions.utils.Glow;
import net.milkbowl.vault.economy.Economy;

public class Minions extends JavaPlugin implements Listener {

	public static HashMap<UUID, ArrayList<Zombie>> zombies = new HashMap<UUID, ArrayList<Zombie>>();
	public static HashMap<Zombie, Material> blocks = new HashMap<Zombie, Material>();
	private static Minions instance;
	public static Economy econ = null;
	
	public void onEnable() {
		instance = this;
		if (!setupEconomy()) {
			Bukkit.getServer().getConsoleSender().sendMessage(
					String.format("[%s] - Disabled due to no Vault dependency found!", getDescription().getName()));
			getServer().getPluginManager().disablePlugin(this);
			return;
		}
		getCommand("Minion").setExecutor(new MinionCommand());
		getServer().getPluginManager().registerEvents(this, this);
		getServer().getPluginManager().registerEvents(new Listeners(), this);
		getServer().getPluginManager().registerEvents(new SpeedGui(), this);
		getServer().getPluginManager().registerEvents(new UpgradesGui(), this);
		getServer().getPluginManager().registerEvents(new MultiplierGui(), this);
		saveDefaultConfig();
		
	}

	public void onDisable() {
		for (UUID keys : zombies.keySet()) {
			
			for (Zombie z : zombies.get(keys)) {
				z.remove();
			}
		}
	}

	@EventHandler
	public void onHit(EntityDamageEvent e) {
		for (Player p : Bukkit.getOnlinePlayers()) {
			for (Zombie z : zombies.get(p.getUniqueId())) {
				if (e.getEntity() == z) {
					e.setCancelled(true);
				}
			}
		}
	}

	public static Minions getInstance() {
		return instance;
	}

	private boolean setupEconomy() {
		if (getServer().getPluginManager().getPlugin("Vault") == null) {
			return false;
		}
		RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
		if (rsp == null) {
			return false;
		}
		econ = rsp.getProvider();
		return econ != null;

	}
}
